package com.oreillyauto.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CarpartsController {

    @GetMapping(value="/carparts")
    public String getCarparts() {
        return "carparts";
    }
    
}
